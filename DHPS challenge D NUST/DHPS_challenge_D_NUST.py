GradePoints = [None] * 6

name=input("Please enter your full name: ")
gender=input("Please enter your gender: ")


def setup_student_info():

    for i in range(0,6):
        subject = input("Enter subject: ")
        OLorHL = input("Subject on HL or OL: ")
        grade = input("Input subject grade: ")
        if OLorHL == "OL":
            if grade == "A+":
                GradePoints[i] = 8
            elif grade == "A":
                GradePoints[i] = 7
            elif grade == "B":
                GradePoints[i] = 6
            elif grade == "C":
                GradePoints[i] = 5
            elif grade == "D":
                GradePoints[i] = 4
            elif grade == "E":
                GradePoints[i] = 3
            elif grade == "F":
                GradePoints[i] = 2
            elif grade == "G":
                GradePoints[i] = 1
            else:
                GradePoints[i] = 0
        else:
            if grade == "1":
                GradePoints[i] = 10
            elif grade == "2":
                GradePoints[i] = 9
            elif grade == "3":
                GradePoints[i] = 8
            elif grade == "4":
                GradePoints[i] = 7
            else:
                GradePoints[i] = 0
    print(GradePoints)
        

def add_GradePoints():
    lowest = 11
    TotalPoints = 0
    for i in range(0,6):
        if GradePoints[i] < lowest:
            lowest = GradePoints[i]
        TotalPoints = TotalPoints + GradePoints[i]

    TotalPoints = TotalPoints - lowest
    return TotalPoints


def evaluate_field_of_study(Total_Points):
    print("___________________________________________")
    print("1  Management Sciences")
    print("2  Human Sciences")
    print("3  Computing and Informatics")
    print("4  Engineering Sciences")
    print("5  Health and Applied Sciences")
    print("6  Natural Resources and Spatial Sciences")
    print("___________________________________________")

    subject_choice = input("Choose number from the list above: ")
    if subject_choice == '1':
        english_grade = input("Enter grade for English: ")
        maths_grade = input("Enter grade for Mathematics: ")
        if (Total_Points > 25 and english_grade != "E" and english_grade != "F" and english_grade != "G" and english_grade != "U"and maths_grade != "F" and maths_grade != "G" and maths_grade != "U"):
            print("Congratulations, you qualify for admission to NUST for your field of choice")
        else:
            print("Sorry, you do not qualify for admission to NUST for your field of choice")
    elif subject_choice == '2':
        english_grade = input("Enter grade for English: ")
        if (Total_Points > 25 and english_grade != "D" and english_grade != "E" and english_grade != "F" and english_grade != "G" and english_grade != "U"):
            print("Congratulations, you qualify for admission to NUST for your field of choice")
        else:
            print("Sorry, you do not qualify for admission to NUST for your field of choice")
    elif subject_choice == '3':
        maths_grade = input("Enter grade for Mathematics: ")
        if (Total_Points > 30 and maths_grade != "D" and maths_grade != "E" and maths_grade != "F" and maths_grade != "G" and maths_grade != "U"):
            print("Congratulations, you qualify for admission to NUST for your field of choice")
        else:
            print("Sorry, you do not qualify for admission to NUST for your field of choice")
    elif subject_choice == '4':
        maths_grade = input("Enter grade for Mathematics: ")
        english_grade = input("Enter grade for English: ")
        physical_science_grade = input("Enter grade for Physical science: ")
        if (Total_Points > 37 and maths_grade != "4" and maths_grade != "U" and english_grade != "U" and physical_science_grade != "U"):
            print("Congratulations, you qualify for admission to NUST for your field of choice")
        else:
            print("Sorry, you do not qualify for admission to NUST for your field of choice")
    elif subject_choice == '5':
        maths_grade = input("Enter grade for Mathematics: ")
        english_grade = input("Enter grade for English: ")
        biology_grade = input("Enter grade for Biology: ")
        physical_science_grade = input("Enter grade for Physical science: ")
        if (Total_Points > 30 and english_grade != "D" and english_grade != "E" and english_grade != "F" and english_grade != "G" and english_grade != "U" and biology_grade != "E" and biology_grade != "F" and biology_grade != "G" and biology_grade != "U"
             and maths_grade != "E" and maths_grade != "F" and maths_grade != "G" and maths_grade != "U" and physical_science_grade != "E" and physical_science_grade != "F" and physical_science_grade != "G" and physical_science_grade != "U"):
            print("Congratulations, you qualify for admission to NUST for your field of choice")
        else:
            print("Sorry, you do not qualify for admission to NUST for your field of choice")
    elif subject_choice == '6':
        maths_grade = input("Enter grade for Mathematics: ")
        english_grade = input("Enter grade for English: ")
        geography_grade = input("Enter grade for Geography: ")
        if (Total_Points > 30 and english_grade != "D" and english_grade != "E" and english_grade != "F" and english_grade != "G" and english_grade != "U"and maths_grade != "F" and maths_grade != "G" and maths_grade != "U" and
            maths_grade != "E" and maths_grade != "F" and maths_grade != "G" and maths_grade != "U" and geography_grade != "E" and geography_grade != "F" and geography_grade != "G" and geography_grade != "U"):
            print("Congratulations, you qualify for admission to NUST for your field of choice")
        else:
            print("Sorry, you do not qualify for admission to NUST for your field of choice")


setup_student_info()
Total_Points = add_GradePoints()
print("Total points: ", Total_Points)
evaluate_field_of_study(Total_Points)
